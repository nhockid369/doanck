const express = require('express');
const router = express.Router();
const mPro = require('../models/product');
const mCat = require('../models/Category');
const mSubCat = require('../models/SubCat');

router.get('/', async (req,res)=>{
    const cats = await mCat.all();
    const subcats = await mSubCat.all();
    for (let cat of cats){
        cat.isActive = false;
    }
    cats[0].isActive = true;
    res.render('shop/homeproduct', {
        title: 'cat test',
        cats: cats,
        subcats: subcats,
    });
});

router.get('/', (req,res,next)=>{
    const page = parseInt(req.query.page) || 1;
    mCat.all().then(cats=> {
        const id = cats[0].IDCat || -1;
        res.redirect(`/cat/${id}/products`);
    })
    .catch(err=>next(err));
});

router.get('/:id/products', async (req,res) => {
    const id = parseInt(req.params.id);
    const page = parseInt(req.query.page) || 1;
    const cats = await mCat.all();
    const rs = await mPro.allByCatId(id, page);
    for (let cat of cats){
        cat.isActive = false;
        if (cat.IDCat === id) {
            cat.isActive = true;
        }
    }
    const pages = [];
    for (let i = 0; i < rs.pageTotal; i++){
        pages[i]= {value: i+1, active: (i+1)===page};
    }
    const navs = {};
    if (page>1){
        navs.prev = page-1;
    }
    if (page < rs.pageTotal){
        navs.next = page+1;
    }
    res.render('shop/homeproduct', {
        title: 'cat test',
        cats: cats,
        ps: rs.products,
        pages: pages,
        navs: navs,
    });
});

module.exports = router;