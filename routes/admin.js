const express = require('express');
const router = express.Router();
const mCat = require('../models/Category');

router.post('/edit', async (req,res,next)=>{
    const id = parseInt(req.body.IDCat);
    const [cats,err] = await run(mCat.all());
    if (err){
        return next(err);
    }
    for (const cat of cats){
        cat.isEdit = false;
        if (cat.IDCat === id){
            cat.isEdit = true;
        }
    }
    res.render('./admin/category',{
        layout: 'admin',
        pageTitle: 'Categories',
        cats,
    });
});
router.post('/del', async (req,res,next)=>{
    const id = parseInt(req.body.IDCat);
    const [nr,err] = await run(mCat.del(id));
    if (err){
        return next(err);
    }
    res.redirect('/admin/cat');
});