const express = require('express');
const router = express.Router();
const hash = require('../utils/hash');
const accountM = require('../models/account.M');
const passport = require('../utils/passport');
const mCat = require('../models/Category');
const mSubCat = require('../models/SubCat');

router.get('/signup', async (req,res)=>{
    const cats = await mCat.all();
    const subcats = await mSubCat.all();
    for (let cat of cats){
        cat.isActive = false;
    }
    cats[0].isActive = true;
    res.render('users/signupform', {
        title: 'cat test',
        cats: cats,
        subcats: subcats,
    });
});

router.get('/login', async (req,res)=>{
    res.redirect('/');
});
router.get('/register', (req,res)=>{
    res.redirect('/');
});
router.get('/profile', (req,res)=>{
    if (!req.session.user){
        res.redirect('/account/login');
    }
    res.redirect('/');
});

router.post('/login', async (req,res)=>{
    const username = req.body.username;
    const password = req.body.password;
    const user = await accountM.getByUsername(username);
    if (user===null){
        res.redirect('/');
        return;
    }
    const pwDb = users.Passw;
    if (hash.comparePassword(password,pwDb)){
        req.session.user = user.IDUser;
        res.redirect('/');
    }
});

router.post('/login', (req,res,next)=>{
    passport.authenticate('local', (err,user,info)=>{
        if (err){
            return next(err);
        }
        if (!user){
            return res.render('account/Login', {
                layout: 'main',
                message: info.message,
            });
        }
        req.login(user, err =>{
            if (err){
                return next(err);
            }
            return res.redirect('/');
        });
    })(req,res,next);
});

router.get('/signout', (req,res)=>{
    if (!req.user){
        res.redirect('/account/Login');
    }
    else {
        req.logOut();
        res.redirect('/');
    }
});

router.post('/signup', async (req,res)=>{
    const username = req.body.Username;
    const password = req.body.Password;
    const pwHash = hash.getHashWithSalt(password);
    const user = {
        Username: username,
        Passw: pwHash,
        RealName: req.body.HoTen,
        Email: req.body.Email,
        Address: req.body.Address,
        Privilege: 0,
    };
    const uId = await accountM.add(user);
});

router.get('/profile', (req,res)=>{
    if (!req.session.uid){
        res.redirect('/signup');
    }
    else {
        res.render('users/userprofile',{
            layout: 'account',
        });
    }
});

module.exports = router;