const passport = require('passport'), 
LocalStrategy = require('passport-local').Strategy, 
accountM = require('../models/account.M'),
funcs = require('../utils/db'),
run = funcs.errorHandle,
hash = require('../utils/hash');

passport.serializeUser(function (user,done) {
    done(null, user.IDUser);
});

passport.deserializeUser( async (id, done) => {
    const [user,err] = await run(accountM.getById(id));
    done(err,user);
});

passport.use(new LocalStrategy(
    async (username, password, done)=>{
        const [user,err] = await run(accountM.getByUsername(username));
        if (err){
            return done(err);
        }
        if (user===null){
            return done(null,false,{message: 'Incorrect username.' });
        }
        if (!hash.comparePassword(password, user.f_Password)){
            return done(null,false,{message: 'Incorrect password. '});
        }
        return done(null,user);
    }
));
