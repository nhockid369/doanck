const mysql = require('mysql');
function createConnection(){
    return mysql.createConnection({
        host:'localhost',
        port: '3306',
        user: 'root',
        password: 'djsmemay123',
        database: 'auctiondb'
    });
}

exports.load = sql =>{
    return new Promise((resolve, reject)=>{
        const con = createConnection();
        con.connect(err=>{
            if (err) {
                reject(err);
            }
        });
        con.query(sql, (error, results, fields)=>{
            if (error) {
                reject(error);
            }
            resolve(results);
        });
        con.end();
    });
};

exports.add = (tbName, entity) =>{
    return new Promise((resolve, reject)=> {
        const con = createConnection();
        con.connect(err=>{
            if (err){
                reject(err);
            }
        });
        const sql = `INSERT INTO ${tbName} SET ?`;
        con.query(sql, entity, (error, results, fields)=> {
            if (error){
                reject(error);
            }
            resolve(results.insertId);
        });
        con.end();
    });
};

exports.del = (tbName, idField, id)=>{
    return new Promise((resolve, reject)=>{
        const con = createConnection();
        con.connect(err=>{
            if (err) {
                reject(err);
            }
        });
        let sql = 'DELETE FROM ?? WHERE ?? = ? ';
        const param = [tbName, idField, id];
        sql = mysql.format(sql, param);
        con.query(sql, (error, results, fields)=>{
            if (error) {
                reject(error);
            }
            else {
                resolve(results.affectedRows);
            }
        });
        con.end();  
    });
};

exports.update = (tbName, idField, entity)=> {
    return new Promise((resolve, reject) => {
        const con = createConnection();
        con.connect(err => {
            if (err) {
                reject (err);
            }
        });
        const id = entity[idField];
        delete entity[idField];
        let sql = `UPDATE ${tbName} SET ? WHERE ${idField} = ${id}`;
        con.query(sql, entity, (error, results, fields) => {
            if (error) {
                reject(error);
            }
            else {
                resolve(results.changedRows);
            }
        });
        con.end();
    });
};

exports.errorHandle = promise => {
    return promise.then(data => [data, undefined])
    .catch(err=> [undefined,err]);
}