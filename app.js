const homeRouter = require('./routes/home');
const accountRouter = require('./routes/account');
const userRouter = require('./routes/user');
const session = require('express-session');
const express = require('express'),
app = express(),
bodyParser = require('body-parser');
port = 3000,
exphbs = require('express-handlebars');
app.use(bodyParser.urlencoded({ extended: false}));
app.use(bodyParser.json());
app.use(session({
    secret: 'abcabc',
    resave: false, 
    saveUninitialized: true,
}));
const path = require('path');
//config handlebars
const hbs = exphbs.create({
    defaultLayout: 'main',
    extname: 'hbs',
});
const regiHbs = require('handlebars');
regiHbs.registerHelper('ifEquals', function(arg1, arg2, options) {
    return (arg1 == arg2) ? options.fn(this) : options.inverse(this);
});
app.engine('hbs', hbs.engine);
app.set('view engine','hbs');
app.set('views', path.join(__dirname, 'public/views'));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/', homeRouter);
app.use('/profile', userRouter);
app.use('/account', accountRouter);
require('./middlewares/passport')(app);

require('./middlewares/errors')(app);
app.listen(port,() => console.log(`Example app listening on port ${port}!`));