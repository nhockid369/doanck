const db = require('../utils/db'),
run = db.errorHandle;
const tbName = "Category", idField = 'IDCat';

module.exports = {
    all: async () => {
        const sql = `SELECT * FROM ${tbName}`;
        const [rows,err] = await run(db.load(sql));
        if (err){
            throw err;
        }
        return rows;
    },
    del: async id =>{
        const [nr,err] = await run(db.update(tbName, idField, entity));
        if (err){
            throw err;
        }
        return nr;
    },
    add: async entity =>{
        const [id,err] = await run(db.add(tbName, entity));
        if (err) {
            throw err;
        }
        return id;
    }
};

