const db = require('../utils/db');
tbName = 'Products';
pageSize = 10;
module.exports = {
    all: async() =>{
        const sql = `SELECT * FROM ${tbName}`;
        const rows = await db.load(sql);
        return rows;
    },
    allByCatId: async id =>{
        const sql = `SELECT * FROM ${tbName} WHERE CatID=${id}`;
        const rows = await db.load(sql);
        return rows;
    },
    allByCatIdPaging : async (id,page) => {
        let sql = `SELECT count(*) AS total FROM ${tbName} WHERE IDCat = ${id}`;
        const rs = await db.load(sql);
        const totalP = rs[0].total;
        const pageTotal = Math.floor(totalP / pageSize)+1;
        const offset = (page-1)*pageSize;
        sql = `SELECT * FROM ${tbName} WHERE IDCat = ${id} LIMIT ${pageSize} OFFSET ${offset}`;
        const rows = await db.load(sql);
        return {
            pageTotal: pageTotal,
            products: rows
        }
    }
};